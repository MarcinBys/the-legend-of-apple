﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GameController : MonoBehaviour {

    //Score
    private int score;
    public Text scoreText;

    //Lifes
    private int lifes;
    public Text lifesText;
    private bool selectLevel = false;

    //Props
    public GameObject cage;

    //Win/Lose text
    public Text notificationText;
    public Text restartText;

    //Local multiplayer
    public GameObject[] players;

    //Level Management
    private LevelManager levelManager;

    //Sounds
    private AudioSource audioSource;

    [Header("Audio Clips")]
    public AudioClip ballSound;
    public AudioClip lifeSound;
    public AudioClip keySound;
    public AudioClip appleSound;
    public AudioClip enemyDeathSound;

    void Start ()
    {
        levelManager = GameObject.FindWithTag("LevelManager").GetComponent<LevelManager>();
        audioSource = GetComponent<AudioSource>();

        if (!GlobalControl.Instance.firstStart)     //if the game is not started for the first time, load player data
        {
            score = GlobalControl.Instance.score;
            lifes = GlobalControl.Instance.lifes;
        }

        UpdateScore();
        SetLifes();

        notificationText.text = "";
        restartText.enabled = false;
    }
	
	void Update () {
        if (!players[0].activeInHierarchy || !players[1].activeInHierarchy)
        {
            restartText.enabled = true;
        }

        if (Input.GetKeyDown(KeyCode.R) && selectLevel)
        {
            SavePlayer();

            if (lifes <= 0)
            {
                levelManager.LoadLevel("Defeat");
            }
            else if (GlobalControl.Instance.castleApple && GlobalControl.Instance.forestApple && GlobalControl.Instance.icelandApple && GlobalControl.Instance.volcanoApple)
            {
                levelManager.LoadLevel("Victory");
            }
            else
            {
                levelManager.LoadLevel("Level_Select");
            }

        }
    }

    private void SavePlayer()
    {
        GlobalControl.Instance.lifes = lifes;
        GlobalControl.Instance.score = score;
    }

    /// <summary>
    /// Method for updating score text.
    /// </summary>
    /// <param name="tag"> Object tag to add points to score (default "") </param>
    public void UpdateScore(string tag="")
    {
        if (tag.Equals("Enemy"))
        {
            score += 231;
            audioSource.clip = enemyDeathSound;
            audioSource.Play();
        }
        else if (tag.Equals("Pickup"))
        {
            score += 100;
            audioSource.clip = ballSound;
            audioSource.Play();
        }
        else if (tag.Equals("Apple"))
        {
            score += 1000;
            audioSource.clip = appleSound;
            audioSource.Play();

            if (levelManager.CheckSceneName("Castle"))
            {
                GlobalControl.Instance.castleApple = true;
            }
            else if (levelManager.CheckSceneName("Forest"))
            {
                GlobalControl.Instance.forestApple = true;
            }
            else if (levelManager.CheckSceneName("Iceland"))
            {
                GlobalControl.Instance.icelandApple = true;
            }
            else if (levelManager.CheckSceneName("Volcano"))
            {
                GlobalControl.Instance.volcanoApple = true;
            }
        }
        else if (tag.Equals("Life"))
        {
            score += 125;
            audioSource.clip = lifeSound;
            audioSource.Play();
        }
        else if (tag.Equals("Key"))
        {
            score += 200;
            audioSource.clip = keySound;
            audioSource.Play();
        }

        scoreText.text = score.ToString();
    }

    /// <summary>
    /// Adds / subtracts number of player's lifes
    /// </summary>
    /// <param name="number"> Default 0 to just update UI text </param>
    public void SetLifes(int number=0)
    {
        lifes += number;

        lifesText.text = lifes.ToString();
    }

    public void Victory()
    {
        players[0].GetComponent<PlayerController>().enabled = false;
        players[1].GetComponent<PlayerController>().enabled = false;

        players[0].GetComponent<Rigidbody2D>().velocity = Vector2.zero;
        players[1].GetComponent<Rigidbody2D>().velocity = Vector2.zero;

        notificationText.color = Color.green;
        notificationText.text = "Victory!";
        restartText.enabled = true;
        selectLevel = true;
    }

    public void Death()
    {
        players[0].GetComponent<PlayerController>().AnimatorDeath();
        players[1].GetComponent<PlayerController>().AnimatorDeath();

        players[0].GetComponent<PlayerController>().enabled = false;
        players[1].GetComponent<PlayerController>().enabled = false;

        if (!selectLevel)
        {
            SetLifes(-1);
            selectLevel = true;
        }

        notificationText.color = Color.red;
        notificationText.text = "You died!";
        restartText.enabled = true;
    }
}