﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class LevelManager : MonoBehaviour
{
    /// <summary>
    /// Method used to check if name of loaded scene contains given string.
    /// </summary>
    /// <param name="scene"> Fragment of scene name. </param>
    /// <returns> Returns true if scene name contains given string. </returns>
    public bool CheckSceneName(string scene)
    {
        if (SceneManager.GetActiveScene().name.Contains(scene))
        {
            return true;
        }

        return false;
    }

    public void LoadLevel(string name)
    {
        Debug.Log("Level load requested for: " + name);
        SceneManager.LoadScene(name);
    }

    public void QuitRequest()
    {
        Debug.Log("Quit requested.");
        Application.Quit();
    }
}