﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerController : MonoBehaviour {

    //Moving
    [Range(0, 10)] public float moveSpeed;
    private float sprintSpeed;
    [SerializeField] private bool facingRight = true;
    private Rigidbody2D rb;

    //Jumping
    [Range(0, 10)] public float jumpVelocity = 5;
    [SerializeField] private float fallingMultiply = 2.25f;      //2.25 for normal gravity levels
    [SerializeField] private float softJumpMultiply = 1f;      //1 for normal gravity levels
    
    //Jumping with Raycast
    [HideInInspector] public bool grounded = false;
    [SerializeField] private LayerMask groundingObjects;
    [SerializeField] private Transform rayLeft, rayMiddle, rayRight; //rayFront;

    //Other
    private GameController gameController;

    //Local multiplayer
    public string jumpButton = "Jump_P1";
    public string horizontalButtons = "Horizontal_P1";
    public string sprintButton = "Sprint_P1";

    //Animations
    private Animator myAnimator;

    //Audio
    private AudioSource audioSource;
    public AudioClip jumpSound;
    public AudioClip deathSound;

    void Start () {
        rb = GetComponent<Rigidbody2D>();
        myAnimator = GetComponent<Animator>();
        audioSource = GetComponent<AudioSource>();

        GameObject gameControllerObject = GameObject.FindWithTag("GameController");
        if (gameControllerObject != null)
        {
            gameController = gameControllerObject.GetComponent<GameController>();
        }
        if (gameControllerObject == null)
        {
            Debug.Log("Cannot find 'GameController' script.");
        }

        if (!facingRight)
        {
            Flip();
        }

        sprintSpeed = moveSpeed * 1.25f;
    }
	
	// Update is called once per frame
	void Update () {
        GravityEnhancement();
        Raycast();

        if (Input.GetButtonDown(jumpButton) && grounded)
        {
            Jump();
        }
    }

    void FixedUpdate()
    {
        Move();

        AnimatorAir();
    }

    private void Move()
    {
        //Input
        float move = Input.GetAxis(horizontalButtons);   //GetAxisRaw for no delay when starting to move

        //Physics
        if (Input.GetButton(sprintButton))
        {
            rb.velocity = new Vector2(move * sprintSpeed, rb.velocity.y);       //sprint
        }
        else
        {
            rb.velocity = new Vector2(move * moveSpeed, rb.velocity.y);         //regular move
        }

        //Animations
        myAnimator.SetFloat("speed", Mathf.Abs(move));

        if (rb.velocity.y < 0)
        {
            myAnimator.SetBool("landing", true);
        }

        //Flipping player's character
        if (move > 0 && !facingRight)
            Flip();
        else if (move < 0 && facingRight)
            Flip();
    }

    private void Jump()
    {
        audioSource.clip = jumpSound;
        audioSource.Play();
        rb.velocity = Vector2.up * jumpVelocity;
        myAnimator.SetTrigger("jump");
    }

    // Used to make jumping/falling more arcade style
    private void GravityEnhancement()
    {
        if (rb.velocity.y < 0 && rb.velocity.y > -50)
        {
            rb.velocity += Vector2.up * Physics2D.gravity.y * Time.deltaTime * fallingMultiply;
        }
        else if (rb.velocity.y > 0 && !Input.GetButton(jumpButton))
        {
            rb.velocity += Vector2.up * Physics2D.gravity.y * Time.deltaTime * softJumpMultiply;
        }
        else if (rb.velocity.y < -50)
        {
            rb.velocity = new Vector2(0, -50);
        }
    }

    // Used to flip player character when facing left or right
    private void Flip()
    {
        facingRight = !facingRight;
        Vector2 localScale = transform.localScale;
        localScale.x *= -1;
        transform.localScale = localScale;
    }

    // Used to detect if player is on the ground or in air, or if something is blocking his movement
    private void Raycast()
    {
        RaycastHit2D hitL = Physics2D.Raycast(rayLeft.position, Vector2.down, 0.1f, groundingObjects);
        RaycastHit2D hitM = Physics2D.Raycast(rayMiddle.position, Vector2.down, 0.1f, groundingObjects);
        RaycastHit2D hitR = Physics2D.Raycast(rayRight.position, Vector2.down, 0.1f, groundingObjects);

        if (hitM || hitL || hitR)
        {
            grounded = true;
        }
        else if (!hitM || !hitL || !hitR)
        {
            grounded = false;
        }

        //Raycasts preview in debug
        if (grounded)
        {
            Debug.DrawRay(rayLeft.position, Vector2.down, Color.red, 0);
            Debug.DrawRay(rayMiddle.position, Vector2.down, Color.red, 0);
            Debug.DrawRay(rayRight.position, Vector2.down, Color.red, 0);
        }
    }

    private void OnTriggerEnter2D(Collider2D other)
    {
        if (other.gameObject.CompareTag("Enemy") && other.transform.position.y < transform.position.y)
        {
            rb.velocity = Vector2.up * jumpVelocity * 1.4f;
        }
        else if (other.gameObject.CompareTag("Pickup"))
        {
            gameController.UpdateScore(other.gameObject.tag);
            other.gameObject.SetActive(false);
        }
        else if (other.gameObject.CompareTag("Life"))
        {
            gameController.UpdateScore(other.gameObject.tag);
            other.gameObject.SetActive(false);
            gameController.SetLifes(1);
        }
        else if (other.gameObject.CompareTag("Key"))
        {
            gameController.UpdateScore(other.gameObject.tag);
            gameController.cage.SetActive(false);
            other.gameObject.SetActive(false);
        }
        else if (other.gameObject.CompareTag("Apple"))
        {
            gameController.UpdateScore(other.gameObject.tag);
            other.gameObject.SetActive(false);
            gameController.Victory();
        }
        else if (other.gameObject.CompareTag("Trap"))
        {
            audioSource.clip = deathSound;
            audioSource.volume = 1;
            audioSource.Play();
            gameController.Death();
        }
    }

    private void AnimatorAir()
    {
        if (!grounded)
        {
            myAnimator.SetLayerWeight(1, 1);
        }
        else
        {
            myAnimator.ResetTrigger("jump");
            myAnimator.SetBool("landing", false);
            myAnimator.SetLayerWeight(1, 0);
        }
    }

    public void AnimatorDeath()
    {
        myAnimator.SetLayerWeight(1, 0);
        myAnimator.SetTrigger("death");
    }
}
