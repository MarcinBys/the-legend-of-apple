﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Spawner : MonoBehaviour {

    public Transform spawnerPosition;       //position of (for example) Empty Object, where objectPrefab will be spawned
    public GameObject objectPrefab;         //prefab of object to spawn

    private void Start()
    {
        InvokeRepeating("Spawn", 0, 3);     //repeat "Spawn" method every 3 seconds
    }

    private void Spawn()
    {
        Vector2 position = new Vector2(spawnerPosition.transform.position.x, spawnerPosition.transform.position.y);
        GameObject spawnObject = Instantiate(objectPrefab, position, Quaternion.identity);
    }
}
