﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Enemy : MonoBehaviour {

    protected GameController gameController;    // protected - zeby moc korzystac ze zmiennej w klasach dziedziczacych po tej
    protected Rigidbody2D rb;
    protected Vector2 initialPosition;  // saves enemy's initial position
    protected bool facingRight = true;

    [Range(0, 10)] public float speed = 1;
    
    void Awake () {         // Jako ze klasa nie jest uzywana w obiekcie, zamiast metody Start trzeba uzyc Awake (inaczej sie nie zaladuje gameController)
        gameController = GameObject.FindWithTag("GameController").GetComponent<GameController>();
        rb = GetComponent<Rigidbody2D>();

        initialPosition = new Vector2(transform.position.x, transform.position.y);
    }

    /// <summary>
    /// How player reacts to this object (enemy)
    /// </summary>
    /// <param name="other"> Player's collider </param>
    void OnTriggerEnter2D(Collider2D other)
    {
        if (other.transform.position.y > transform.position.y && other.gameObject.CompareTag("Player"))
        {
            gameController.UpdateScore(gameObject.tag);
            Destroy(gameObject);
        }
        else if (other.gameObject.CompareTag("Player"))
        {
            gameController.Death();
        }
    }

    protected void Flip()
    {
        facingRight = !facingRight;
        Vector2 localScale = transform.localScale;
        localScale.x *= -1;
        transform.localScale = localScale;
    }
}