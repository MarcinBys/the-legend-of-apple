﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Spider : Enemy {

    private Vector2 move;
    public bool moveRight = true;
    public float jumpRepeatRate = 2.0f;

    [Range(0, 10)] public float range = 1;
    [Range(0, 10)] public float jumpVelocity = 5;

    //Raycasts
    [SerializeField] private LayerMask visibleObjects;
    [SerializeField] private Transform rayMiddle;

    void Start()
    {
        InvokeRepeating("Jump", Random.Range(1, 9), jumpRepeatRate);

        if (!moveRight)
        {
            Flip();
        }
    }

    void FixedUpdate()
    {
        Move();
    }

    /// <summary>
    /// Method used to move spider left and right and jump on max left side and max right side.
    /// </summary>
    private void Move()
    {
        if (transform.position.x >= initialPosition.x + (1 * range))
        {
            moveRight = false;
            Flip();
        }
        else if (transform.position.x <= initialPosition.x - (1 * range))
        {
            moveRight = true;
            Flip();
        }

        if (moveRight)
        {
            move.x = speed * Time.deltaTime;
        }
        else if (!moveRight)
        {
            move.x = -speed * Time.deltaTime;
        }

        transform.Translate(move);
    }

    private void Jump()
    {
        RaycastHit2D hitM = Physics2D.Raycast(rayMiddle.position, Vector2.down, 0.1f, visibleObjects);
        Debug.DrawRay(rayMiddle.position, Vector2.down, Color.red, 0);

        if (hitM)
        {
            rb.velocity = Vector2.up * jumpVelocity;
        }
    }
}