﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Fly : Enemy {
    
    private Vector2 fly;
    private bool flyUp = true;

    [Range(0, 10)] public float range = 1;

    void FixedUpdate()
    {
        Flight();
    }

    /// <summary>
    /// Method used to move fly upwards and downwards.
    /// </summary>
    private void Flight()
    {
        if (transform.position.y >= initialPosition.y + (1 * range))
        {
            flyUp = false;
        }
        else if (transform.position.y <= initialPosition.y - (1 * range))
        {
            flyUp = true;
        }

        if (flyUp)
        {
            fly.y = speed * Time.deltaTime;
        }
        else if (!flyUp)
        {
            fly.y = -speed * Time.deltaTime;
        }

        transform.Translate(fly);
    }
}
