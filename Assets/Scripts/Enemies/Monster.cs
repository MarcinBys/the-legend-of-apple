﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Monster : Enemy {
    
    private Vector2 move;
    public bool moveRight = true;

    //Raycasts
    [SerializeField] private LayerMask visibleObjects;
    [SerializeField] private Transform rayLeft, rayRight;

    void Start()
    {
        if (!moveRight)
        {
            Flip();
        }
    }

    void FixedUpdate()
    {
        Move();
    }

    /// <summary>
    /// Method used to move monster to the left side and right side of a platform.
    /// </summary>
    private void Move()
    {
        RaycastHit2D hitL = Physics2D.Raycast(rayLeft.position, Vector2.down, 0.1f, visibleObjects);
        RaycastHit2D hitR = Physics2D.Raycast(rayRight.position, Vector2.down, 0.1f, visibleObjects);

        //Raycasts preview in debug
        Debug.DrawRay(rayLeft.position, Vector2.down, Color.red, 0);
        Debug.DrawRay(rayRight.position, Vector2.down, Color.red, 0);

        if (!hitL || !hitR)
        {
            moveRight = !moveRight;
            Flip();
        }
        
        if (moveRight)
        {
            move.x = speed * Time.deltaTime;
        }
        else if (!moveRight)
        {
            move.x = -speed * Time.deltaTime;
        }

        transform.Translate(move);
    }
}