﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Buttons : MonoBehaviour {

    public GameObject player;
    public GameObject affectedObject;

    //Affected object variables
    [Range(0, 10)] public float moveRange = 1;
    private Vector2 move;
    private Vector2 initialPosition;

    //Button
    private bool buttonPressed = false;
    private Animator myAnimator;

    //Audio
    private AudioSource audioSource;
    public AudioClip pressSound;

    void Start () {
        myAnimator = GetComponent<Animator>();
        audioSource = GetComponent<AudioSource>();

        initialPosition = new Vector2(affectedObject.transform.position.x, affectedObject.transform.position.y);
    }

    void Update()
    {
        MoveObject();
    }

    private void OnTriggerEnter2D(Collider2D other)
    {
        //if assigned Player pressed button OR if Player pressed button but no one was assigned to it
        if (other.gameObject.Equals(player) || (other.gameObject.CompareTag("Player") && player.Equals(null)))
        {
            buttonPressed = true;
            myAnimator.SetBool("pressed", true);
            audioSource.clip = pressSound;
            audioSource.Play();
        }
    }

    private void OnTriggerExit2D(Collider2D other)
    {
        if (other.gameObject.Equals(player) || (other.gameObject.CompareTag("Player") && player.gameObject.Equals(null)))
        {
            buttonPressed = false;
            myAnimator.SetBool("pressed", false);
        }
    }

    private void MoveObject()
    {
        if (buttonPressed && affectedObject.transform.position.y <= initialPosition.y + (1 * moveRange))
        {
            move.y = Time.deltaTime * 1;
            affectedObject.transform.Translate(move);
        }
        else if (!buttonPressed && affectedObject.transform.position.y >= initialPosition.y)
        {
            move.y = Time.deltaTime * -1;
            affectedObject.transform.Translate(move);
        }
    }
}