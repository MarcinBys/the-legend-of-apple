﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlatformMover : MonoBehaviour {

    private Vector2 initialPosition;
    private Vector2 move;
    [Range(0, 10)] public float moveRange;
    [Range(0, 10)] public float moveSpeed;

    public bool moveHorizontal;
    private bool moveUp;

    public bool moveVertical;
    private bool moveLeft;
    
    void Start () {
        initialPosition = new Vector2(transform.position.x, transform.position.y);
	}
	
	void Update () {
		if (moveHorizontal)
        {
            MoveHorizontally();
        }
        if (moveVertical)
        {
            MoveVertically();
        }
	}

    private void MoveHorizontally()
    {
        if (transform.position.x >= initialPosition.x + (1 * moveRange))
        {
            moveLeft = false;
        }
        else if (transform.position.x <= initialPosition.x - (1 * moveRange))
        {
            moveLeft = true;
        }

        if (moveLeft)
        {
            move.x = moveSpeed * Time.deltaTime;
        }
        else if (!moveLeft)
        {
            move.x = -moveSpeed * Time.deltaTime;
        }

        transform.Translate(move);
    }

    private void MoveVertically()
    {
        if (transform.position.y >= initialPosition.y + (1 * moveRange))
        {
            moveUp = false;
        }
        else if (transform.position.y <= initialPosition.y - (1 * moveRange))
        {
            moveUp = true;
        }

        if (moveUp)
        {
            move.y = moveSpeed * Time.deltaTime;
        }
        else if (!moveUp)
        {
            move.y = -moveSpeed * Time.deltaTime;
        }

        transform.Translate(move);
    }

    private void OnCollisionEnter2D(Collision2D other)
    {
        other.transform.SetParent(transform);
    }

    private void OnCollisionExit2D(Collision2D other)
    {
        other.transform.SetParent(null);
    }
}
