﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Teleport : MonoBehaviour {

    void OnTriggerEnter2D(Collider2D other)
    {
        if (CompareTag("Boundary"))
        {
            //Teleport player to the left/right side of the screen
            if (other.transform.position.x > 0)
            {
                other.transform.position = new Vector2(-other.transform.position.x + 0.1f, other.transform.position.y); // +0.1f x offset to not glitch on the collider
            }
            else if (other.transform.position.x < 0)
            {
                other.transform.position = new Vector2(-other.transform.position.x - 0.1f, other.transform.position.y); // -0.1f x offset to not glitch on the collider
            }
        }
    }

    void OnTriggerExit2D(Collider2D other)
    {
        if (CompareTag("LowerBoundary"))
        {
            //Teleport player to the upper side of the screen
            other.transform.position = new Vector2(other.transform.position.x, -other.transform.position.y);
        }
    }
}
