﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SelectController : MonoBehaviour {

    public Text selectText;
    public Text lifesText;
    private LevelManager levelManager;

    public int moveSpeed = 4;
    private bool facingRight = true;
    private Rigidbody2D rb;
    public string collision;
    
    //Animations
    private Animator myAnimator;

    void Start () {
        if (!GlobalControl.Instance.firstStart)     //if the game is not started for the first time, load level selector object position
        {
            transform.position = new Vector2(GlobalControl.Instance.posX, GlobalControl.Instance.posY);
        }

        levelManager = GameObject.FindWithTag("LevelManager").GetComponent<LevelManager>();
        rb = GetComponent<Rigidbody2D>();
        myAnimator = GetComponent<Animator>();

        lifesText.text = GlobalControl.Instance.lifes.ToString();
        selectText.text = "";
    }
	
	void Update ()
    {
        Move();

        if (Input.GetKey(KeyCode.Space) && !collision.Equals(""))
        {
            SelectLevel(collision);
        }
	}

    private void Move()
    {
        float moveHorizontal = Input.GetAxisRaw("Horizontal_P1");
        float moveVertical = Input.GetAxisRaw("Vertical");
        
        rb.velocity = new Vector2(moveHorizontal * moveSpeed, moveVertical * moveSpeed);

        myAnimator.SetFloat("speed", Mathf.Abs(moveHorizontal + moveVertical));

        //Flipping player's character
        if (moveHorizontal > 0 && !facingRight)
            Flip();
        else if (moveHorizontal < 0 && facingRight)
            Flip();
    }

    // Used to flip player character when facing left or right
    private void Flip()
    {
        facingRight = !facingRight;
        Vector2 localScale = transform.localScale;
        localScale.x *= -1;
        transform.localScale = localScale;
    }

    private void SelectLevel(string levelName)
    {
        SavePosition();
        
        if (levelName.Contains("Castle") && !GlobalControl.Instance.castleApple)
        {
            levelManager.LoadLevel("Level_Castle");
        }
        else if (levelName.Contains("Forest") && !GlobalControl.Instance.forestApple)
        {
            levelManager.LoadLevel("Level_Forest");
        }
        else if (levelName.Contains("Iceland") && !GlobalControl.Instance.icelandApple)
        {
            levelManager.LoadLevel("Level_Iceland");
        }
        else if (levelName.Contains("Volcano") && !GlobalControl.Instance.volcanoApple)
        {
            levelManager.LoadLevel("Level_Volcano");
        }
        else
        {
            selectText.text = "Apple found, nothing to do here...";
        }
    }

    private void SavePosition()
    {
        GlobalControl.Instance.posX = transform.position.x;
        GlobalControl.Instance.posY = transform.position.y;
    }

    private void OnTriggerEnter2D(Collider2D other)
    {
        collision = other.gameObject.ToString();
        selectText.text = "Select level by pressing SPACE";
    }

    private void OnTriggerExit2D(Collider2D other)
    {
        collision = "";
        selectText.text = "";
    }
}