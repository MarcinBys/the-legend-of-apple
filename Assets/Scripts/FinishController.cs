﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class FinishController : MonoBehaviour {

    public Text scoreText;
    private int score;

    public GameObject player1;
    public GameObject player2;
    public LevelManager levelManager;

    void Start () {
        try
        {
            score = GlobalControl.Instance.score;
            scoreText.text = "Final score: " + score;
        }
        catch
        {
            Debug.Log("GameObject with GlobalControl script does not exist!");
        }

        Destroy(GameObject.Find("Data Object"));       //Used to destroy undestroyable object holding player data between scenes

        if (levelManager.CheckSceneName("Victory"))
        {
            InvokeRepeating("VictoryScene", 1, 3);
        }
        else if (levelManager.CheckSceneName("Defeat"))
        {
            DefeatScene();
        }
    }

    private void VictoryScene()
    {
        player1.GetComponent<Rigidbody2D>().velocity = Vector2.up * 5;
        player2.GetComponent<Rigidbody2D>().velocity = Vector2.up * 5;
    }

    private void DefeatScene()
    {

    }
}