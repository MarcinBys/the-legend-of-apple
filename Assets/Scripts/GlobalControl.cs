﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GlobalControl : MonoBehaviour {

    public static GlobalControl Instance;

    [HideInInspector] public int lifes;
    [HideInInspector] public int score;
    public bool firstStart = true;

    // Select Level scene positions
    [HideInInspector] public float posX;
    [HideInInspector] public float posY;

    // Level progress
    [HideInInspector] public bool castleApple;
    [HideInInspector] public bool forestApple;
    [HideInInspector] public bool icelandApple;
    [HideInInspector] public bool volcanoApple;

    void Awake()        //Awake jest wczytywane przed wszystkimi innymi metodami (Start, Update itd.)
    {
        if (Instance == null)
        {
            DontDestroyOnLoad(gameObject);
            Instance = this;
        }
        else if (Instance != this)
        {
            Destroy(gameObject);
        }

        if (firstStart)
        {
            lifes = 5;
            score = 0;
            posX = 0;
            posY = 2;
            castleApple = false;
            forestApple = false;
            icelandApple = false;
            volcanoApple = false;
            firstStart = !firstStart;
        }
    }
}